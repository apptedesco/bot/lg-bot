<h1 align="center">Welcome to LG Bot 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
  <a href="https://twitter.com/steodec" target="_blank">
    <img alt="Twitter: steodec" src="https://img.shields.io/twitter/follow/steodec.svg?style=social" />
  </a>
</p>

## Install

```sh
yarn install
```

## Progression
* WareWolf Configratoion
> ▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱ 50%
* Party
>▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱▱ 0%
* Multi Server
> ▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰▰ 100%
## Author

👤 **steodec**

* Website: [https://paul-tedesco.com](https://paul-tedesco.com)
* Twitter: [@steodec](https://twitter.com/steodec)
* Github: [@PaulTedesco](https://github.com/PaulTedesco)
* Gitlab: [@steodec](https://gitlab.com/steodec)
* LinkedIn: [@Tedesco Paul](https://linkedin.com/in/tedesco-p-972aa7ba)

## Show your support

Give a ⭐️ if this project helped you!

***