-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 25 mars 2020 à 13:48
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `lgbot`
--

-- --------------------------------------------------------

--
-- Structure de la table `guild`
--
create TABLE IF NOT EXISTS `guild`
(
    `guild_id` varchar(255) NOT NULL unique,
    `prefix`   varchar(255) NOT NULL DEFAULT '!',
    `lang`     varchar(255) NOT NULL DEFAULT 'en',
    PRIMARY KEY (`guild_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

create TABLE IF NOT EXISTS `party`
(
    `id`          varchar(255) NOT NULL DEFAULT uuid(),
    `Villageois`  varchar(255)          DEFAULT NULL,
    `LoupGarous`  varchar(255)          DEFAULT NULL,
    `Voleur`      varchar(255)          DEFAULT NULL,
    `Cupidon`     varchar(255)          DEFAULT NULL,
    `Amoureux`    varchar(255)          DEFAULT NULL,
    `Voyante`     varchar(255)          DEFAULT NULL,
    `PetiteFille` varchar(255)          DEFAULT NULL,
    `Sorcière`    varchar(255)          DEFAULT NULL,
    `Capitaine`   varchar(255)          DEFAULT NULL,
    `Chasseur`    varchar(255)          DEFAULT NULL,
    `Salvateur`   varchar(255)          DEFAULT NULL,
    `Idiot`       varchar(255)          DEFAULT NULL,
    `Bouc`        varchar(255)          DEFAULT NULL,
    `Ancien`      varchar(255)          DEFAULT NULL,
    `Flute`       varchar(255)          DEFAULT NULL,
    `LoupBlanc`   varchar(255)          DEFAULT NULL,
    `Sauvage`     varchar(255)          DEFAULT NULL,
    `Renard`      varchar(255)          DEFAULT NULL,
    `Servante`    varchar(255)          DEFAULT NULL,
    `Soeur`       varchar(255)          DEFAULT NULL,
    `Ours`        varchar(255)          DEFAULT NULL,
    `Comedien`    varchar(255)          DEFAULT NULL,
    `Chevalier`   varchar(255)          DEFAULT NULL,
    `Juge`        varchar(255)          DEFAULT NULL,
    `Ange`        varchar(255)          DEFAULT NULL,
    `nbrPlayer`   int                   DEFAULT NULL,
    `guild_id`    varchar(255)          DEFAULT NULL,
    `mj`          varchar(255)          DEFAULT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (guild_id)
        REFERENCES guild (guild_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

create TABLE IF NOT EXISTS `lg`
(
    `party_id` varchar(255) NOT NULL,
    `nbrLoups` int(11)      NOT NULL,
    `Loup1`    varchar(255) DEFAULT NULL,
    `Loup2`    varchar(255) DEFAULT NULL,
    `Loup3`    varchar(255) DEFAULT NULL,
    `Loup4`    varchar(255) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
COMMIT;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
