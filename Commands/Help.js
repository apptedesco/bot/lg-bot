/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');

module.exports = class Help extends Commands {
    constructor(client) {
        super(client, {
            name: "help",
            description: "get help for all commande",
            usage: `${client.config.prefix}help (commands)`,
            aliases: ["h", "?"]
        });
    }

    async run(message, args) {
        let name = [];
        let value = [];
        await this.client.commands.forEach(cmd => {
            name.push(cmd.help.name);
            value.push(`${cmd.help.description}\n${this.client.config.langMessage.HELP.USAGE}: \`${cmd.help.usage}\``)
        });
        await message.author.send(super.messageEmbed(name, value, "ba3b3b",
            this.client.config.langMessage.HELP.HELP, true))

    }
};