/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');

let count = 0;
let cancel = false;
let partyID = "";
module.exports = class StartGame extends Commands {
    constructor(client) {
        super(client, {
            name: "start",
            description: "delete message",
            usage: `${client.config.prefix}start`,
            aliases: ["st", "sg"]
        })

    }

    async run(message, args) {
        const [row] = await this.client.config.connect.query(
            `SELECT id FROM party WHERE mj = ${message.author.id} LIMIT 1`);
        if (row.length > 0) {
            await message.reply(this.client.config.langMessage.GAME.ERROR.PARTY);
            return
        }
        await this.setCount(message);
        if (cancel) return;
        await this.setLoupGarou(message);
        if (cancel) return;
        await this.setCard(message);
    }

    async setLoupGarou(message) {
        let nbrLoup = 0;
        await message.channel.send(this.messageEmbed([this.client.config.langMessage.GAME.CONFIGURATION.CONF,
                this.client.config.langMessage.GAME.CONFIGURATION.NBRPLAYER],
            [this.client.config.langMessage.GAME.CONFIGURATION.NBRWOLF,
                `${count}`], "ba3b3b")).then(
            async msg => {
                msg.react('1️⃣');
                if (count > 2)
                    msg.react('2️⃣');
                if (count > 4 * 2) {
                    msg.react('3️⃣');
                    msg.react('4️⃣');
                }
                msg.react('❌');
                const filter = (reaction, user) => {
                    return ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '❌'].includes(reaction.emoji.name) && user.id === message.author.id
                };
                await msg.awaitReactions(filter, {max: 1}).then(async col => {
                        let react = col.first();
                        switch (react.emoji.name) {
                            case '1️⃣':
                                nbrLoup = 1;
                                break;
                            case '2️⃣':
                                nbrLoup = 2;
                                break;
                            case '3️⃣':
                                nbrLoup = 3;
                                break;
                            case '4️⃣':
                                nbrLoup = 4;
                                break;
                            case '❌':
                                message.reactions.resolve(message.author.id);
                                cancel = true;
                                this.check = true;
                                await msg.delete();
                                await this.client.config.connect.query(`DELETE FROM party WHERE id = ?`, [partyID]);
                                await message.reply(this.client.config.langMessage.GAME.CONFIGURATION.CANCEL);
                                return;

                        }
                        await this.client.config.connect.query(`INSERT INTO lg (party_id, nbrLoups) VALUE (?, ?)`,
                            [partyID, nbrLoup])
                    }
                )
            })
    }

    async setCount(message) {
        this.check = false;
        while (!this.check) {
            await message.channel.send(this.messageEmbed([this.client.config.langMessage.GAME.CONFIGURATION.CONF,
                    this.client.config.langMessage.GAME.CONFIGURATION.DIC,
                    this.client.config.langMessage.GAME.CONFIGURATION.NBRPLAYER],
                [`${this.client.config.langMessage.GAME.CONFIGURATION.NBRPLAYER} ?`,
                    this.client.config.langMessage.GAME.CONFIGURATION.DICVAL, `${count}`], "ba3b3b")).then(async msg => {
                msg.react('⏩');
                msg.react('⏭');
                msg.react('⏪');
                msg.react('⏮');
                msg.react('✅');
                msg.react('❌');
                const filter = (reaction, user) => {
                    return ['⏩', '⏪', '⏮', '⏭', '✅', '❌'].includes(reaction.emoji.name)
                        && user.id === message.author.id
                };
                await msg.awaitReactions(filter, {max: 1}).then(async col => {
                    let react = col.first();
                    switch (react.emoji.name) {
                        case '⏩':
                            count = count + 1;
                            message.reactions.resolve(message.author.id);
                            await msg.delete();
                            break;
                        case '⏭':
                            count = count + 5;
                            message.reactions.resolve(message.author.id);
                            await msg.delete();
                            break;
                        case '⏮':
                            count = count - 5;
                            message.reactions.resolve(message.author.id);
                            await msg.delete();
                            break;
                        case '⏪':
                            count = count - 1;
                            message.reactions.resolve(message.author.id);
                            await msg.delete();
                            break;
                        case '✅':
                            await message.reactions.removeAll();
                            await this.client.config.connect.query(`INSERT INTO party(guild_id, mj, nbrPlayer) 
                                VALUE ((SELECT id FROM guild 
                                WHERE guild_id = ${message.guild.id} LIMIT 1), ${message.author.id}, ${count})`);
                            let [row, field] = await this.client.config.connect.query(`SELECT id 
                                FROM party where mj = ?`,
                                [message.author.id]);
                            partyID = row[0].id;
                            console.log(partyID);
                            this.check = true;
                            break;
                        case '❌':
                            message.reactions.resolve(message.author.id);
                            cancel = true;
                            this.check = true;
                            await msg.delete();
                            await message.reply(this.client.config.langMessage.GAME.CONFIGURATION.CANCEL);
                            break;
                    }
                });
            })
        }
    }

    async setCard(message) {

    }
};



