/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');
const config = require('../Config/config');
const lang = require(`./lang`);

module.exports = class Purge extends Commands {
    constructor(client) {
        super(client,
            {
                name: "purge",
                description: "delete message",
                usage: `${client.config.prefix}purge [1-100]`,
                aliases: ["delete", "remove"]
            });
    }

    async run(message, args) {
        const deleteCount = parseInt(args[0]);
        if (!deleteCount || deleteCount < 1 || deleteCount > 100)
            return message.reply(this.client.config.langMessage.ERROR.PURGE);
        message.channel.bulkDelete(deleteCount)
            .catch(error => message.reply(`${error}`));
    }
};

