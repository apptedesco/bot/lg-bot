/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');
const config = require('../Config/config');
const lang = require(`./lang`);

module.exports = class StopGame extends Commands {
    constructor(client) {
        super(client, {
            name: "stop",
            description: "delete game",
            usage: `${client.config.prefix}stop`,
            aliases: ["gameStop", "sp"]
        });
    }

    async run(message, args) {
        const [row, field] = await this.client.config.connect.query(`SELECT id FROM party WHERE mj = ? LIMIT 1`,
            message.author.id);
        if (row.length === 0) {
            await message.reply(this.client.config.langMessage.GAME.ERROR.NOPARTY);
            return
        } else {
            message.channel.send(this.messageEmbed([this.client.config.langMessage.GAME.CONFIGURATION.CONF],
                [this.client.config.langMessage.GAME.CONFIGURATION.DELETEPARTY], "ba3b3b")).then(async msg => {
                await msg.react('✅');
                await msg.react('❌');
                const filter = (reaction, user) => {
                    return ['❌', '✅'].includes(reaction.emoji.name) && user.id === message.author.id
                };
                await msg.awaitReactions(filter, {max: 1}).then(async col => {
                    let react = col.first();
                    if (react.emoji.name === '✅') {
                        message.reactions.resolve(message.author.id);
                        await msg.delete();
                        const [row] = await this.client.config.connect.query('SELECT id FROM party WHERE mj = ?',
                            [message.author.id]);
                        await this.client.config.connect.query('DELETE FROM party WHERE mj = ?', [message.author.id]);
                        await this.client.config.connect.query(`DELETE FROM lg WHERE party_id = ?`, row[0].id);
                        await message.reply(this.client.config.langMessage.GAME.CONFIGURATION.CANCEL)

                    }

                })
            })
        }
    };
};



