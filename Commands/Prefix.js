/**
 * @author: steodec
 * @copyright: Steodec
 */
const edit = require('edit-json-file');
const filePath = './Config/Config.json';
const Commands = require('../Base/Commands.js');

module.exports = class Lang extends Commands {
    constructor(client) {
        super(client, {
            name: "prefix",
            description: "change prefix",
            usage: `${client.config.prefix}prefix (!, ? ...)`,
            aliases: ["p"]
        })

    }

    async run(message, args) {
        if (args[0]) {
            this.client.config.connect.query(`UPDATE guild
                                              SET prefix = ?
                                              WHERE guild_id = ?`, [args[0], message.guild.id]);
            this.client.loadPrefix(message.guild.id);
            await message.reply(this.client.config.langMessage.CONFIG.PREFIX)
        }
    }
};