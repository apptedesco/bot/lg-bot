/**
 * @author: steodec
 * @copyright: Steodec
 */
const edit = require('edit-json-file');
const filePath = './Config/Config.json';
const Commands = require('../Base/Commands.js');

module.exports = class Lang extends Commands {
    constructor(client) {
        super(client, {
            name: "lang",
            description: "change language",
            usage: `${client.config.prefix}lang (fr, eu)`,
            aliases: ["l"]
        })

    }

    async run(message, args) {
        if (args[0] === 'fr' || args[0] === "en") {
            this.client.config.connect.query(`UPDATE guild
                                              SET lang = ?
                                              WHERE guild_id = ?`, [args[0], message.guild.id]);
            this.client.loadLang(`${this.client.config.path.lang}`, message.guild.id);
            await message.reply(this.client.config.langMessage.CONFIG.LANG)
        }
    }
};