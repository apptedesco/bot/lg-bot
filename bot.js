/**
 * @author: steodec
 * @copyright: Steodec
 */

const Client = require('./Base/Client');
const config = './Config/Config';

const bot = new Client({config: config});

bot.login(bot.config.token);
bot.loadDataBase(bot.config.database);

bot.loadCommands(bot.config.path.commands);
bot.loadEvents(bot.config.path.events);

