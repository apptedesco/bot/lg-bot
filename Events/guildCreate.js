module.exports = class {
    constructor(client) {
        this.client = client;
    }

    run(guild) {
        console.log(`New guild added : ${guild.name}, owned by ${guild.owner.user.username}`);
        this.client.config.connect.query(`INSERT INTO guild (guild_id) VALUES(${guild.id})`)
    }
};