module.exports = class {
    constructor(client) {
        this.client = client;
    }

    async run(message) {
        await this.client.loadLang(this.client.config.path.lang, message.guild.id);
        await this.client.loadPrefix(message.guild.id);
        if (message.author.bot || !message.content.startsWith(this.client.config.prefix)) return;
        const args = message.content.split(/\s+/g);
        const command = args.shift().slice(this.client.config.prefix.length);
        const cmd = this.client.commands.get(command) || this.client.commands.get(this.client.aliases.get(command));

        if (!cmd) return;

        cmd.setMessage(message);
        cmd.run(message, args);

        message.delete();

        if (cmd.conf.cooldown > 0) cmd.startCooldown(message.author.id);
    }
};