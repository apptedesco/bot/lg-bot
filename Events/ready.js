const {readFileSync} = require('fs');
module.exports = class {
    constructor(client) {
        this.client = client;
    }

    run() {
        let sqlFile = readFileSync('./SQL/initTable.sql').toString();
        this.client.config.connect.query(sqlFile);
        console.info(`LG-Bot: Logged is as ${this.client.user.tag}`);
        this.client.config.connect.query(`SELECT COUNT(guild_id) as total
                                          FROM guild`, (err, result) => {
            this.client
                .user.setActivity(`Loups Garous (${result[0].total} guild(s))`, {type: "PLAYING"});
        })
    };
};